
// console.log("Siesta Time!");

//Array Methods

// JavaScript has built-in functions and methods for arrays.
// This allows us to manipulate and access array ElementInternals.

// Mutator methods - are built-in functions that mutate or change the array after they are created.
// These methods manipulate the original array performing various tasks as adding and removing elements


let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log(fruits);
    // push()
    /* -Adds an element in the end of an array and returns the array's length.
    -Syntax: arrayName.push(elementsToBeAdded);
*/

console.log("CurrentArray fruits[]:");
console.log(fruits);

fruits.push('Mango');
console.log("Mutated array from push method: ");
console.log(fruits);


let removedFruit = fruits.pop();
console.log("Mutated Array after the pop method: ");
console.log(removedFruit);


console.log("Current Array fruits[]: ");
console.log(fruits);


// insert elements in the first index of an array.
// returns the present length.
console.log(fruits.unshift('Lime', 'Banana'));
console.log("Mutated Array after the unshift() method: ");
console.log(fruits);


// shift()
/*

    -removes an element at the beginning of an array AND it returns the removed element.
    -Syntax: 
        arrayName.shift()

*/
console.log(fruits.shift('Lime'));
console.log("Mutated Array after the shift() method: ");
console.log(fruits);



/*
Splice() - simultaneously removes elements from a specified index and adds elements. 
    Syntax: 
        arrayName.splice(startinginde, deletecount, element to be added)


*/

console.log("Mutated Array after the shift() method: ");
console.log(fruits);

fruits.splice(1,1, "Lime");
console.log(fruits);


let index = 3;


console.log(fruits);

console.log(fruits.splice(3,1));
console.log(fruits.splice(3,0, "Durian", "Santol"));
console.log(fruits);

//sort()
    /*
        -Rearranges the array elements in alphanumeric order
        -syntax:
        arrayName.sort();
    */

console.log("Current Array fruits[]:");
console.log(fruits);
fruits.sort();

console.log("Mutated Array after the sort() method: ");
console.log(fruits);

// Important note for the sort method is used for more complicated functions
// focus the bootcampers on the basic usage of the sort method.

//reverse()
    /*
        -reverses the order of array elements
        arrayName.reverse();
    */

fruits.reverse();
console.log("Mutated reverse sort")
console.log(fruits);

// Non-mutator methods
/*
Non-mutator methods are functions that do not modify or change an array after they are created.
-These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output.

*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

//indexOf()
/*
    -it returns the index number of the first matching element found in an array.
    -if no match was found, the result will be -1
    -The search process will be done from first element proceeding to the last element.

    Syntax:

    arrayName.indexOf(searchValue);
    arrayname.indexof(searchValue, startingIndex);

*/

let country;

console.log("array of Non-mutator method");
console.log(countries.indexOf('PH'));
console.log("Index of PH country after the index 1: ", + countries.indexOf('PH',3));
console.log(countries.indexOf('BR'));

// lastIndexOf()
/*
    -returns the index number of the last matching element found in an array
    -the serach process will be done from last element until the first element.

*/
console.log(countries.lastIndexOf('PH'));
// slice()
/* portions/slices from an array and returns a new array
// Syntax:
// arrayName.slice(startingIndex);
// arrayName.slice(startingIndex, endingIndex)-endingIndex is not included in the returned array.

// Slicing of elements from a specified index to the last element.
*/

let slicedArray = countries.slice(2, 5);
console.log(slicedArray);

// Slicing off elements starting from the last element of an array.
let slicedArrayC = countries.slice(-3, -1);
console.log(slicedArrayC);


// toString()
/*
    -returns an array as a string separated by commas
    -syntax 
    arrayName.toString()

*/

let stringedArray = countries.toString();

console.log(stringedArray);

//concat()
    /*
        combines two arrays and returns the combined result.
        -syntax: 
        arrayA.concat(arrayB);
        arrayA.concat(element);
    */

let tasksArrayA = ['drink HTML', 'eat javascript'];
let tasksArrayB = ['inhale CSS', 'breathe SASS'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log(tasks.concat(tasksArrayC,"here"));

//Combine arrays with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');

console.log("Result from concat method: ");
console.log(combinedTasks);
